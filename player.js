function initMIDIPlayer(soundfontUrl){
	soundfontUrl = soundfontUrl || "./bower_components/llmidi/soundfont/"; 
	MIDI.loadPlugin({
		soundfontUrl: soundfontUrl,
		callback: function() {
			MIDI.delay_ = 0; // play one note every quarter second
			MIDI.velocity_ = 127; // how hard the note hits
		}
	});
};

function playNote(note){
	note = note.replace('/','');
	MIDI.setVolume(0, 512);
	note = MIDI.keyToNote[note];
	MIDI.noteOn(0, note, MIDI.velocity_, MIDI.delay_);
	MIDI.noteOff(0, note, MIDI.delay_ + 0.75);	
};

